﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PubSubPattern
{
    /// <summary>
    /// A simple Pub/Sub pattern implementation.
    /// </summary>
    public sealed class PubSubService
    {
        // Explicit static constructor to tell C# compiler
        // not to mark type as beforefieldinit
        static PubSubService()
        {
        }

        private PubSubService()
        {
        }

        /// <summary>
        /// A singleton for service interaction.
        /// </summary>
        public static PubSubService Instance { get; } = new PubSubService();
        private static Dictionary<string, PublishEventHandler> AllPublishEvents = new Dictionary<string, PublishEventHandler>();
        private delegate void PublishEventHandler(string channel, object data);
        
        /// <summary>
        /// Clear the event handlers
        /// </summary>
        public void ClearEventHandlers()
        {
            AllPublishEvents.Clear();
        }

        /// <summary>
        /// Subscribes a given IHandleMessage implementation to the channels it returns.
        /// </summary>
        /// <param name="implementation">An instance of IHandleMessage.</param>
        /// <exception cref="ArgumentNullException">Throws ArgumentNullException if implementation is null</exception>
        public void Subscribe(IHandleMessage implementation)
        {
            if (implementation == null) throw new ArgumentNullException("implementation is null");

            implementation.GetSubscribedChannels().ForEach(channel =>
            {
                AddPublishEventHandlerForKeyIfItDoesntExist(channel);
                var PublishEvent = AllPublishEvents[channel];

                var implementationHasAlreadyBeenSubscribed = PublishEvent != null && PublishEvent
                                                            .GetInvocationList()
                                                            .Any(handler =>
                                                                handler.Target?.GetHashCode() == implementation.GetHashCode());

                if (!implementationHasAlreadyBeenSubscribed)
                    AllPublishEvents[channel] += implementation.HandleMessage;
            });
        }

        private void AddPublishEventHandlerForKeyIfItDoesntExist(string key)
        {
            if (!AllPublishEvents.ContainsKey(key))
            {
                PublishEventHandler publishEventHandler = null;

                AllPublishEvents.Add(key, publishEventHandler);
            }
        }

        /// <summary>
        /// Unsubscribes a given IHandleMessage implementation to the channels it returns.
        /// </summary>
        /// <param name="implementation">An instance of IHandleMessage.</param>
        /// <exception cref="ArgumentNullException">Throws ArgumentNullException if implementation is null</exception>
        public void Unsubscribe(IHandleMessage implementation)
        {
            if (implementation == null) throw new ArgumentNullException("implementation is null");

            implementation.GetSubscribedChannels().ForEach(channel =>
            {
                if (AllPublishEvents.ContainsKey(channel))
                {
                    AllPublishEvents[channel] -= implementation.HandleMessage;
                }
            });
        }

        /// <summary>
        /// Publishes a message to a given channel containing the specified data.
        /// </summary>
        /// <param name="channel">The channel to emit a message on.</param>
        /// <param name="data">The data to emit.</param>
        /// <exception cref="ArgumentNullException">Throws ArgumentNullException if channel is null.</exception>
        /// <exception cref="ArgumentNullException">Throws ArgumentNullException if data is null.</exception>
        public void Publish(string channel, object data)
        {
            if (string.IsNullOrEmpty(channel)) throw new ArgumentNullException("channel is null");
            if (data == null) throw new ArgumentNullException("data is null");
            if (!(data is Lead)) throw new ArgumentException("data is not type Lead");

            var lead = data as Lead;
            if (lead.FirstName == null) throw new ArgumentException("FirstName is null or empty");

            if (AllPublishEvents.ContainsKey(channel))
            {
                var publishEventHandler = AllPublishEvents[channel];
                publishEventHandler?.Invoke(channel, data);
            }
        }
    }
}
